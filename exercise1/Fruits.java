package exercise1;

import java.util.List;

public class Fruits {
    public static void main(String[] args) {
        List<String> fruitList = List.of("Apple", "Banana","Guava","Pineapple");

//        printBasic(fruitList);
        printWithFP(fruitList);
    }

        private static void printBasic(List<String> fruitList){
            for (String string : fruitList){
                System.out.println(string);
            }
        }

    private static void printWithFP(List<String> fruitList){
        fruitList.stream().forEach(
                element -> System.out.println(element) // Lambda expression // action
        );

    }

}
