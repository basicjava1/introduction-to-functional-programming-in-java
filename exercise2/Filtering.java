package exercise2;

import java.util.List;

public class Filtering {
    public static void main(String[] args) {
        List<String> animalList = List.of("Cat", "Bat","Tiger","Dog");
        printWithFilter(animalList);
        printFilterFP(animalList);
    }
    private static void printWithFilter(List<String> animalList){
        for(String string : animalList){
            if(string.endsWith("at"))
                System.out.println(string);
        }
    }

    private static void printFilterFP(List<String> animalList){
        animalList.stream()
                .filter(element -> element.endsWith("at"))
                .forEach(
                element -> System.out.println(element) // Lambda expression // action
        );
    }

}
