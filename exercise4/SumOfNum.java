package exercise4;

import java.util.List;

public class SumOfNum {
    public static void main(String[] args) {
        List<Integer> integers = List.of(6,3,4,2,6,7,8,4,7);

//        int sum = 0;
//        for(int integer : integers)
//            sum += integer;
        int sum = integers.stream().reduce(0, (number1, number2) -> number1+number2);

        System.out.println(sum);
    }
}
