package exercise6;

import java.util.List;

public class MethodReference {

    public static void print(Integer number){
        System.out.println(number);
    }

    public static void main(String[] args) {
        List.of("Cat","Bat", "Fog").stream()
                .map(s -> s.length())
                .forEach(s -> print(s));

        List.of("Ant","Spider","Tigrer").stream()
                .map(s -> s.length()) // .map(String::length)
                .forEach(MethodReference::print);

        /* Integer max = List.of(23,45,76,55).stream()
                .filter(n -> n % 2 == 0)
                .max((n1, n2) -> Integer.compare(n1, n2))
                .orElse(0);
        System.out.println(max); */

        Integer max = List.of(32,34,63,24).stream()
                .filter(MethodReference::isEven)
                .max(Integer::compare)
                .orElse(0);
        System.out.println(max);
    }
    public static boolean isEven(Integer num){
        return num%2 == 0;
    }
}
