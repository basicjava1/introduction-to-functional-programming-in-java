package exercise5;

import java.util.List;
import java.util.stream.IntStream;

public class ToLowerCase {
    public static void main(String[] args) {
        lowerCasePrint();
    }

    static void lowerCasePrint(){
        List.of("Apple", "Cat", "Dog","Car")
                .stream()
                        .map(s->s.toLowerCase())
                                .forEach(p->System.out.println(p));

    }
}
