package exercise5;

import java.util.List;
import java.util.stream.IntStream;

public class Squares {
    public static void main(String[] args) {
        squarePrint();
    }

    static void squarePrint(){
        IntStream.range(1,10)
                .forEach(
                        e -> System.out.println("Squares : "+e*e)
                );
    }

}
