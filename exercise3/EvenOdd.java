package exercise3;

import java.util.List;

public class EvenOdd {
    public static void main(String[] args) {
        List<Integer> numberList = List.of(5,3,6,7,1,2,4,8);
        printOdd(numberList);
        printEven(numberList);
    }

    private static void printOdd(List<Integer> numberList){
        numberList.stream()
                .filter(element -> element % 2 == 1)
                .forEach(
                        element -> System.out.println("Odd: "+element) // Lambda expression // action
                );
    }
    private static void printEven(List<Integer> numberList){
        numberList.stream()
                .filter(element -> element % 2 == 0)
                .forEach(
                        element -> System.out.println("Even " +element) // Lambda expression // action
                );
    }

}
